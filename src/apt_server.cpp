//-------- C++ specific headers>
#include <algorithm>
#include <cstdint>  // uint8_t (8 bits) and uint16_t (16 bits)
#include <cstdlib>  // <stdlib.h>
#include <cstring>  // <string.h>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

//------ Poxix/ U-nix specific header files ------//
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/signal.h>

enum class SockStatus {
  Success,
  HostFailure,  // DNS failure
  CannotOpenSocket,
  ConnectionFailure,
  BindError
};

auto statusToString(SockStatus s) -> const char* {
  using E = SockStatus;
  if (s == E::Success) return "Success";
  if (s == E::HostFailure) return "Host Failure / DNS Failure";
  if (s == E::CannotOpenSocket) return "Cannot opern socket";
  if (s == E::ConnectionFailure) return "Connection Failure";
  if (s == E::BindError) return "Bind error";
  return nullptr;
}

/** Encapsulates BSD-like sockets found in BSD-variants, Linux, OSX, Android and
 * so on. */
class BSDSocket {
 private:
  int m_sockfd;

 public:
  using ConnHandler = std::function<void(BSDSocket&)>;

  BSDSocket() {}
  BSDSocket(const BSDSocket&) = delete;
  BSDSocket& operator=(const BSDSocket&) = delete;

  BSDSocket(BSDSocket&& rhs) { rhs.m_sockfd = std::move(m_sockfd); }

  BSDSocket& operator=(BSDSocket&& rhs) {
    this->m_sockfd = std::move(rhs.m_sockfd);
    return *this;
  }

  ~BSDSocket() {
    std::perror(" [TRACE] Socket closed OK");
    ::close(m_sockfd);
  }

  int getDescriptor() { return m_sockfd; }

  auto getAddress() -> const char* {
    sockaddr_in peer;
    int plen = sizeof(peer);
    if (::getpeername(m_sockfd, (sockaddr*)&peer, (socklen_t*)&plen) == -1)
      return "";
    return inet_ntoa(peer.sin_addr);
  }

  auto getPort() -> int {
    sockaddr_in peer;
    int plen = sizeof(peer);
    if (::getpeername(m_sockfd, (sockaddr*)&peer, (socklen_t*)&plen) == -1)
      return -1;
    return static_cast<int>(ntohs(peer.sin_port));
  }
  auto close() -> void { ::close(m_sockfd); }

  /** Connect to some hostname and port - set up client socket */
  auto connect(uint16_t port, const std::string& hostname = "127.0.0.1")
      -> SockStatus {
    struct addrinfo hints;
    struct addrinfo *servinfo, *p;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    int status = ::getaddrinfo(hostname.c_str(), std::to_string(port).c_str(),
                               &hints, &servinfo);
    if (status != 0) {
      std::cout << gai_strerror(status) << std::endl;
      return SockStatus::HostFailure;
    }

    int enable_reuseaddr = 1;
    for (p = servinfo; p != nullptr; p = p->ai_next) {
      m_sockfd = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if (m_sockfd < 0) {
        std::cout << "No socket" << std::endl;
        continue;
      }

      if (::connect(m_sockfd, p->ai_addr, p->ai_addrlen) < 0) {
        std::cout << "connect error" << std::endl;
        continue;
      }
      break;
    }

    freeaddrinfo(servinfo);
    if (p == nullptr) {
      std::cout << "Client failed to connect" << std::endl;
      return SockStatus::BindError;
    }

    std::cout << "Return sockstatus success" << std::endl;
    return SockStatus::Success;
  }

  /** Set a socket server. */
  auto bind(uint16_t port, const std::string& hostname = "0.0.0.0")
      -> SockStatus {
    struct addrinfo hints;
    struct addrinfo *servinfo, *p;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int status = ::getaddrinfo(hostname.c_str(), std::to_string(port).c_str(),
                               &hints, &servinfo);
    if (status != 0) {
      std::cout << gai_strerror(status) << std::endl;
      return SockStatus::HostFailure;
    }

    int enable_reuseaddr = 1;
    for (p = servinfo; p != nullptr; p = p->ai_next) {
      m_sockfd = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if (m_sockfd < 0) {
        continue;
      }

      if (::setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr,
                       sizeof(int)) < 0) {
        return SockStatus::BindError;
      }

      if (::bind(m_sockfd, p->ai_addr, p->ai_addrlen) < 0) {
        continue;
      }
      break;
    }

    freeaddrinfo(servinfo);
    if (p == nullptr) {
      return SockStatus::BindError;
    }

    return SockStatus::Success;
  }

  /** Start listening  port. */
  auto listen(int connections, bool useThread, ConnHandler handler) -> void {
    ::listen(m_sockfd, connections);

    struct sockaddr_storage client;
    socklen_t sin_size;
    while (true) {
      sin_size = sizeof(client);
      int clientSock =
          accept(m_sockfd, reinterpret_cast<sockaddr*>(&client), &sin_size);

      if (clientSock < 0)
        throw std::runtime_error("Error: failed to receive socket connection");
      auto so = BSDSocket();
      so.m_sockfd = clientSock;
      if (useThread)
        std::thread([&so, handler]() { handler(so); });
      else
        handler(so);
    }
  }

  /** Check whether the connection is alive by trying to connect to client.  */
  auto isAlive() -> bool {
    char buffer[1];
    int n = ::recv(m_sockfd, buffer, 1, MSG_PEEK);
    // std::fprintf(stderr, " [TRACE] n = %d\n", n);
    return n > 0;
  }
  /** Send binary data */
  auto send(size_t size, const char* buffer) -> int {
    int n = ::send(m_sockfd, buffer, size, 0);
    if (n < 0)
      std::perror(" [ERROR] Sending message to server.");
    else if (n == 0)
      std::perror(" [ERROR] Server closed.");
    return n;
  }
  /** Receive binary data */
  auto recev(size_t size, char* buffer) -> void {
    ::recv(m_sockfd, buffer, size, 0);
    return;
  }
  auto sendText(const std::string& data) -> int {
    int n = ::send(m_sockfd, data.c_str(), data.size(), 0);
    if (n < 0)
      std::perror(" [ERROR] Sending message to server.");
    else if (n == 0)
      std::perror(" [ERROR] Server closed.");
    return n;
  }
  auto sendTextLine(const std::string& line) -> int {
    return this->sendText(line + "\n");
  }
  auto recvText(size_t size) -> std::string {
    std::string buffer(size, 0);
    ::recv(m_sockfd, (char*)buffer.data(), size, 0);
    return buffer;
  }
};

void sigintHandler(int sig_num) 
{ 
  std::cout <<("\n Cannot be terminated using Ctrl+C \n")<< std::endl;
} 
auto runAPT() -> bool {
  BSDSocket server;
  server.bind(65123, "127.0.0.1");
  syslog(LOG_NOTICE, "APTServer waiting for connections");
  server.listen(10, false, [](BSDSocket& client) {
    auto host = client.getAddress();
    auto port = client.getPort();
    syslog(LOG_NOTICE, "Connection from %s:%d",host, port);
    
    client.sendTextLine(std::string("Welcome to APT Server."));
    client.sendText(std::string("apt > "));
    std::string line;
    while (client.isAlive()) {
      line = client.recvText(1024);
      syslog(LOG_NOTICE, line.c_str());
      client.sendText(line);
      client.sendText(std::string("apt > "));
    }
    return true;
  });

  syslog(LOG_NOTICE, "Server finished");
  return true;
}

auto runAsDaemon(const std::string cwd, bool verbose,
                 std::function<bool()> action) -> int {
  pid_t child_pid = fork();
  if (child_pid < 0) {
    std::exit(EXIT_FAILURE);
  }
  if (child_pid > 0) {
    std::exit(EXIT_SUCCESS);
  }

  if (setsid() < 0) std::exit(EXIT_FAILURE);
  signal(SIGCHLD, SIG_IGN);
  signal(SIGHUP, SIG_IGN);

  /* Fork off for the second time*/
  pid_t gchild_pid = fork();

  /* An error occurred */
  if (gchild_pid < 0) exit(EXIT_FAILURE);

  /* Success: Let the parent terminate */
  if (gchild_pid > 0) exit(EXIT_SUCCESS);

  /* Set new file permissions */
  umask(0);

  /* Change the working directory to the root directory */
  /* or another appropriated directory */
  chdir(cwd.c_str());

  while (!action())
    ;
  openlog("apt_server", LOG_PID, LOG_DAEMON);
  return true;
}

int main(int argc, char** argv) {
  if (argc != 2) std::exit(EXIT_SUCCESS);
  signal(SIGINT, sigintHandler);
  if (std::string(argv[1]) == "server") {
    runAsDaemon("/", true, runAPT);
    syslog(LOG_NOTICE, "APT_Server Started");
  }

  return EXIT_SUCCESS;
}
