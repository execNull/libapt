#include <apt.hpp>
#include <apt_msg.hpp>

namespace LIBAPT {

/*
** Constructors and functions for APT_MSG
*/
APT_MSG::APT_MSG(APT_MSG_ID h, size_t s) : header(h) {
  data.reserve(s);
  data.resize(s);
  insertValueInBytes(0, 1, static_cast<int>(header));
  if (s > getEnumValue(APT_MSG_SZ::HEADER)) {
    insertValueInBytes(2, 3, s - getEnumValue(APT_MSG_SZ::HEADER));
    data.at(4) |= 0x80;
  }
}

template <class InputIterator>
APT_MSG::APT_MSG(InputIterator from, InputIterator to) {
  data.assign(from, to);
  uint16_t h;
  extractValueFromBytes(0, 1, h);
  header = static_cast<APT_MSG_ID>(h);
}

//Operator overloading for printing the APT_MSG
auto operator<<(std::ostream& os, const class APT_MSG& msg) -> std::ostream& {
  for (const auto& i : msg.data)
    os << std::hex << std::setfill('0') << std::setw(2)
       << static_cast<int>(i) << ":";
  os << std::dec;

  return os;
}

auto operator+(const std::string& s, const class APT_MSG& msg) -> std::string {
  std::stringstream stream;
  stream << std::hex << getEnumValue(msg.getId());
  std::string result( "0x" + stream.str() );
  return s + result;
}

// Function checking if the Message ID and parameters of two messages are
// the same
auto APT_MSG::operator==(const APT_MSG& other) const -> bool {
  return std::equal(data.begin(), data.begin() + 4, other.data.begin());
}

auto APT_MSG::getId() const -> APT_MSG_ID { return header; }

auto APT_MSG::getName() const -> std::string {
  std::stringstream ss;
  ss << getId();
  return ss.str();
}

auto APT_MSG::setSource(HOSTS source) -> void {
  data[4] |= static_cast<uint8_t>(source);
}

auto APT_MSG::setDestination(HOSTS destination) -> void {
  data[5] |= static_cast<uint8_t>(destination);
}

auto APT_MSG::setSize(size_t newSize) -> void {
  if (newSize > getEnumValue(APT_MSG_SZ::HEADER)) {
    data.resize(newSize);
    insertValueInBytes(2, 3, newSize - getEnumValue(APT_MSG_SZ::HEADER));
    data.at(4) |= 0x80;
  }
}

auto APT_MSG::setSubMsg(APT_SUBMSG_ID id, APT_SUBMSG_SZ sz) -> void {
  if ((header != APT_MSG_ID::PZMOT_SET_PARAMS) &&
      (header != APT_MSG_ID::PZMOT_REQ_PARAMS) &&
      (header != APT_MSG_ID::PZMOT_GET_PARAMS))
    return;
  setSize(getEnumValue(sz));
  insertValueInBytes(6, 7, getEnumValue(id));
}

/*
** APT_DEVICE functions
*/

// Initialization function which sets the parameters for the operation of the
// device. Mainly copied and pasted from the documentation.
auto APTDev::init() -> bool {
  // If the device is already initialized, return
  if (getStatus() == APTDevStatus::Initialized) return true;

  // Check if only one device is connected, if yes a serial number is not
  // needed. Else abort and ask for a serial number.
  if (serialNumber.empty()) {
    std::map<std::string, APTDevType> connectedDevices;
    aptDevScan(&connectedDevices);
    if (connectedDevices.size() == 0) {
      error = "No devices detected. Aborting.";
      return false;
    } else if (connectedDevices.size() > 1) {
      error = "Multiple devices connected. Please initialize with serial number.\n";
      return false;
    }
    // Only one device found, set the serial number
    serialNumber = connectedDevices.begin()->first;
  }

  ftStatus = FT_SetVIDPID(VID, PID);
  if (ftStatus != FT_OK) {
    error = "Unable to set VID & PID combination.";
    return false;
  }

  // Open the device based on the serial number
  ftStatus = FT_OpenEx(const_cast<char*>(serialNumber.c_str()),
		       FT_OPEN_BY_SERIAL_NUMBER,
		       &ftHandle);

  if (ftStatus != FT_OK) {
    error = "Device with serial number "
      + serialNumber
      + " not found. Aborting.";
    return false;
  }

  // EventMask = FT_EVENT_RXCHAR | FT_EVENT_MODEM_STATUS |
  // FT_EVENT_LINE_STATUS;

  // Default values taken from the documentation
  ftStatus = FT_SetBaudRate(ftHandle, 115200);
  ftStatus |= FT_SetDataCharacteristics(ftHandle, FT_BITS_8, FT_STOP_BITS_1,
                                        FT_PARITY_NONE);
  ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX);
  ftStatus |= FT_ResetDevice(ftHandle);
  ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0, 0);
  ftStatus |= FT_SetRts(ftHandle);
  ftStatus |= FT_SetTimeouts(ftHandle, 5000, 5000);
  if (ftStatus != FT_OK) {
    error = "Error during setting of APT device parameters";
    return false;
  }

  // Enable the verbosity
  verbose = false;

  // Activate a thread for sending messages to the device and a thread
  // for receing messages
  processingActive2 = 1;
  deviceMonitorThread = std::thread(&APTDev::deviceMonitor2, this, &processingActive2);

  devStatus = APTDevStatus::Initialized;
  return true;
}

auto APTDev::finalize() -> bool {
  // If the device is not initialized, return
  if (getStatus() != APTDevStatus::Initialized) return true;
  send(LIBAPT::APT_MSG_ID::HW_STOP_UPDATEMSGS);
  // exitThreadsSignal.set_value();
  processingActive2 = 0;
  deviceMonitorThread.join();

  rxBuffer.clear();
  ftStatus = FT_Close(ftHandle);
  if (ftStatus != FT_OK) {
    setError("Unable to close the device.");
    return false;
  }

  devStatus = APTDevStatus::Disabled;
  return true;
}

auto APTDev::setVerbosity(bool v) -> bool {
    verbose = v;
    return true;
}

auto APTDev::send(APT_MSG_ID header) -> bool {
  bool status = false;
  if (messagelist.count(header) != 1) {
    setError("Device does not support message:");
    return false;
  }

  status = write(getMsg(header));
  return status;
}

auto APTDev::write(APT_MSG& msg, HOSTS source, HOSTS destination) -> FT_STATUS {
  DWORD written = 0;
  // Check if msg is malformed as this freezes the device
  int pack_size;
  if ((msg[5] >> 7) == 1) {
    msg.extractValueFromBytes(2, 3, pack_size);
    if (pack_size + getEnumValue(APT_MSG_SZ::HEADER) == msg.size()) {
      setError("Package is malformed, not writing");
      //This needs to be changed
      return ftStatus;
    }
  }
  // Set the source and destination of the messages, then write
  msg.setSource(source);
  msg.setDestination(destination);
  ftStatus = FT_Write(ftHandle, &msg[0], msg.size(), &written);
  if (ftStatus != FT_OK) {
    setError("Could not write message " + msg);
    return ftStatus;
  }
  if (ftStatus == FT_OK && verbose) {
    std::cout << "--> " << msg.getId() << ": " << msg << std::endl;
  }
  return ftStatus;
}

auto APTDev::deviceMonitor(std::future<void> processingActive) -> void {
  rxBuffer.reserve(1024);
  while (processingActive.wait_for(std::chrono::milliseconds(100)) ==
         std::future_status::timeout) {
    checkRxQueue();
  }
}
auto APTDev::deviceMonitor2(int* processingActive) -> void {
  rxBuffer.reserve(1024);
  while (*processingActive) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    checkRxQueue();
  }
}

auto APTDev::checkRxQueue() -> FT_STATUS {
  DWORD rxBytes;
  DWORD bytesReceived;
  BYTE rxBuffers[256];
  ftStatus = FT_GetQueueStatus(ftHandle, &rxBytes);
  // if (ftStatus != FT_OK)  << "ERROR" << std::endl;
  if (rxBytes == 0) return FT_OK;
  ftStatus = FT_Read(ftHandle, rxBuffers, rxBytes, &bytesReceived);

  if (bytesReceived > 0)
    std::copy(&rxBuffers[0], &rxBuffers[bytesReceived],
              std::back_inserter(rxBuffer));

  while (1) {  // Check if a complete header is existing in the buffer
    if (rxBuffer.size() < getEnumValue(APT_MSG_SZ::HEADER)) break;
    // If a complete header exists, check MSB of destination byte for data
    // packet
    int packet_size = 0;
    if ((rxBuffer[4] & 0x80) > 3 == 1) packet_size = rxBuffer.at(2);

    // Check if the buffer contains at least one full message
    if (rxBuffer.size() < getEnumValue(APT_MSG_SZ::HEADER) + packet_size) break;

    // Create a new msg where we know that the complete message is
    APT_MSG msg(
        rxBuffer.begin(),
        rxBuffer.begin() + getEnumValue(APT_MSG_SZ::HEADER) + packet_size);
    rxBuffer.erase(
        rxBuffer.begin(),
        rxBuffer.begin() + getEnumValue(APT_MSG_SZ::HEADER) + packet_size);
    if (verbose) {
      std::cout << "<-- " << msg.getId() << ": " << msg << std::endl;
    }
    if (messagelist.count(msg.getId()) == 1) {
      if (messagelist.at(msg.getId()).action != nullptr)
        messagelist.at(msg.getId()).action(msg);
    }
  }
  return ftStatus;
}

auto APTDev::getError() -> std::string {
  std::string tmpError = error;
  error = "";
  return tmpError;
}

auto APTDev::addMsg(APT_MSG_ID header, size_t s) -> void {
  APT_MSG msg{header, s};
  messagelist.insert(std::map<APT_MSG_ID, APT_MSG>::value_type(header, msg));
}

auto APTDev::getMsg(APT_MSG_ID msgId) -> APT_MSG& {
  if (messagelist.count(msgId) == 1) {
    return messagelist.at(msgId);
  }
}

auto APTDev::addMsgAction(APT_MSG_ID msgId, msgAction action) -> void {
  if (messagelist.count(msgId) == 1) {
    messagelist.at(msgId).addAction(action);
  }
}

APTDev::APTDev() {
  addMsg(APT_MSG_ID::MOD_IDENTIFY);
  addMsg(APT_MSG_ID::HW_START_UPDATEMSGS);
  addMsg(APT_MSG_ID::HW_STOP_UPDATEMSGS);
  addMsg(APT_MSG_ID::HW_REQ_INFO);
  addMsg(APT_MSG_ID::HW_GET_INFO);

  msgAction hwGetInfo = [this](APT_MSG& msg) -> bool {
    msg.extractValueFromBytes(6, 9, hwSerialNumber);
    hwReqInfo = true;
    for (int i = 10; i <= 17; i++) {
      hwModelName.push_back(static_cast<char>(msg[i]));
    }
    msg.extractValueFromBytes(18, 19, hwType);
    msg.extractValueFromBytes(20, 23, hwFirmwareVersion);
    msg.extractValueFromBytes(84, 85, hwHardwareVersion);
    msg.extractValueFromBytes(86, 87, hwModificationState);
    msg.extractValueFromBytes(88, 89, hwNumberChannels);
    hwReqInfoCV.notify_all();
    return true;
  };
  addMsgAction(APT_MSG_ID::HW_GET_INFO, hwGetInfo);
}

APTDev::~APTDev(){};

auto APTDev::getSerialNumber() -> unsigned long {
  return hwSerialNumber;
}

auto APTDev::getModelName() -> std::string{
  return hwModelName;
}

auto APTDev::getHardwareType() -> uint16_t {
  return hwType;
}

auto APTDev::getFirmwareVersion() -> uint32_t {
  // uint major = (ver ) & 0x0000FF;
  // uint interim = (ver & 0x00FF00)  >> 8;
  // uint minor = (ver & 0xFF0000) >> 16;
  return hwFirmwareVersion;
}

auto APTDev::getHardwareVersion() -> uint16_t{
  return hwHardwareVersion;
}

auto APTDev::getModificationState() -> uint16_t{
  return hwModificationState;
}

auto APTDev::getNumberChannels() -> uint16_t {
  return hwNumberChannels;
}

}  // namespace LIBAPT
