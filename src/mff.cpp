#include <apt.hpp>
#include <apt_msg.hpp>
#include <cmath>

namespace LIBAPT {
MFF::MFF(std::string sn) {
  if (serialNumber.empty()) {
    serialNumber = sn;
  }
  devType = APTDevType::MFF;
  addMsg(APT_MSG_ID::MOT_REQ_STATUSBITS);
  addMsg(APT_MSG_ID::MOT_GET_STATUSBITS, 12);
  addMsg(APT_MSG_ID::MOT_MOVE_JOG);
  addMsg(APT_MSG_ID::MOT_REQ_STATUSBITS);
  addMsg(APT_MSG_ID::MOT_SET_MFF_OPERPARAMS, 40);
  addMsg(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  addMsg(APT_MSG_ID::MOT_GET_MFF_OPERPARAMS, 40);
  addMsg(APT_MSG_ID::MOT_REQ_STATUSUPDATE);
  addMsg(APT_MSG_ID::MOT_GET_STATUSUPDATE);

  motGetMFFOperparams = [this](APT_MSG& msg) {
    msg.extractValueFromBytes(8, 11, iTransitTime);
    msg.extractValueFromBytes(12, 15, iTransitTimeADC);
    msg.extractValueFromBytes(16, 17, operMode1);
    msg.extractValueFromBytes(18, 19, sigMode1);
    msg.extractValueFromBytes(20, 23, pulseWidth1);
    msg.extractValueFromBytes(24, 25, operMode2);
    msg.extractValueFromBytes(26, 27, sigMode2);
    msg.extractValueFromBytes(28, 31, pulseWidth2);
    mffOperParamsCV.notify_all();
  };
  addMsgAction(APT_MSG_ID::MOT_GET_MFF_OPERPARAMS, motGetMFFOperparams);

  motGetStatusUpdate = [this](APT_MSG& msg) {
    pos = static_cast<position>(msg[16]);
    if (pos == position::motion) {
      moveInitiated = true;
    }
    if (moveInitiated && pos != position::motion && pos != position::unknown1) {
      moveInitiated = false;
    }
    if (pos != position::motion && pos != position::unknown1) {
      motMoveCV.notify_all();
    }
  };
  addMsgAction(APT_MSG_ID::MOT_GET_STATUSUPDATE, motGetStatusUpdate);
}

auto MFF::init() -> bool {
  if (!APTDev::init()) {
    return false;
  }
  send(LIBAPT::APT_MSG_ID::HW_REQ_INFO);
  // send(LIBAPT::APT_MSG_ID::HW_START_UPDATEMSGS);
  send(LIBAPT::APT_MSG_ID::MOT_REQ_STATUSUPDATE);
  std::this_thread::sleep_for(std::chrono::milliseconds(150));
  return true;
}

MFF::position MFF::getPosition() {
  bool status = false;
  if (getStatus() != APTDevStatus::Initialized) {
    return position::unknown1;
  }
  return pos;
}

bool MFF::motMoveJog(uint8_t channel, MFF::position direction) {
  bool status = false;
  std::unique_lock<std::mutex> lock(moveMutex);
  // motMoveCV.wait(lock);
  // moveMutex.unlock();
  send(LIBAPT::APT_MSG_ID::HW_START_UPDATEMSGS);
  if (getStatus() != APTDevStatus::Initialized) return false;

  if (direction == MFF::position::toggle) {
    direction =
        static_cast<MFF::position>(getEnumValue(pos) ^ getEnumValue(direction));
  } else if (direction == MFF::position::up ||
             direction == MFF::position::down) {
    if (pos == direction) {
      return true;
    }
  } else {
    setError(
        "Position provided is not valid. Acceptable values are 0x01, 0x02 or "
        "0x03.");
    return false;
  }
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_JOG);
  msg.insertValueInBytes(2, channel);
  msg.insertValueInBytes(3, getEnumValue(direction));
  if (write(msg) == FT_OK) {
    status = true;
  }
  motMoveCV.wait(lock, [this, direction] { return pos == direction; });
  send(LIBAPT::APT_MSG_ID::HW_STOP_UPDATEMSGS);
  return status;
}

bool MFF::motSetMFFOperparams() {
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_SET_MFF_OPERPARAMS);
  msg.insertValueInBytes(8, 11, iTransitTime);
  msg.insertValueInBytes(12, 15, iTransitTimeADC);
  msg.insertValueInBytes(16, 17, operMode1);
  msg.insertValueInBytes(18, 19, sigMode1);
  msg.insertValueInBytes(20, 23, pulseWidth1);
  msg.insertValueInBytes(24, 25, operMode2);
  msg.insertValueInBytes(26, 27, sigMode2);
  msg.insertValueInBytes(28, 31, pulseWidth2);
  return write(msg);
}

bool MFF::setITransitTime(uint32_t time) {
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  mffOperParamsCV.wait(lock);
  if (time < iTransitTimeMin || time > iTransitTimeMax) {
    setError("ITransitValue has a valid range between 300 ms and 2800 ms.");
    return false;
  } else {
    iTransitTime = time;
    iTransitTimeADC = 10000000 / pow(iTransitTime, 1.591);
    std::cout << iTransitTimeADC << std::endl;
  }
  return motSetMFFOperparams();
}

bool MFF::setITransitTimeADC(uint32_t time) {
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  mffOperParamsCV.wait(lock);
  if (time < iTransitTimeADCMin || time > iTransitTimeADCMax) {
    setError(
        "ITransitTimeADC has a valid range between 32 (2800ms) and 1145 "
        "(300ms).");
    return false;
  } else {
    iTransitTimeADC = time;
    iTransitTime = pow(10000000 / iTransitTimeADC, 1 / 1.591);
    std::cout << iTransitTime << std::endl;
  }
  return motSetMFFOperparams();
}

bool MFF::setOperMode1(uint16_t mode) { return setOperMode(1, mode); }

bool MFF::setOperMode2(uint16_t mode) { return setOperMode(2, mode); }

bool MFF::setOperMode(int ioChannel, uint16_t mode) {
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  mffOperParamsCV.wait(lock);
  if ((mode < operModeMin) || (mode > operModeMax)) {
    setError("OperMode1 has four possible options: 01, 02, 03, and 04.");
    return false;
  }
  if (ioChannel == 1)
    operMode1 = mode;
  else if (ioChannel == 2)
    operMode2 = mode;

  return motSetMFFOperparams();
}

bool MFF::setSignalMode1(uint16_t mode) { return setSignalMode(1, mode); }

bool MFF::setSignalMode2(uint16_t mode) { return setSignalMode(2, mode); }

bool MFF::setSignalMode(int ioChannel, uint16_t mode) {
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  mffOperParamsCV.wait(lock);
  if (std::find(signalModeValues.begin(), signalModeValues.end(), mode) ==
      signalModeValues.end()) {
    setError(
        "OperMode1 has four possible options: 0x01, 0x02, 0x04, 0x10, 0x20, "
        "and 0x40.");
    return false;
  }
  if (ioChannel == 1)
    sigMode1 = mode;
  else if (ioChannel == 2)
    sigMode2 = mode;

  return motSetMFFOperparams();
}

bool MFF::setPulseWidth1(uint32_t mode) { return setPulseWidth(1, mode); }

bool MFF::setPulseWidth2(uint32_t mode) { return setPulseWidth(2, mode); }

bool MFF::setPulseWidth(int ioChannel, uint32_t width) {
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  moveMutex.lock();
  mffOperParamsCV.wait(lock);
  if (pulseWidthMin < 10 || pulseWidthMax > 200) {
    setError("PulseWidth has a valid range between 10 ms and 200 ms.");
    return false;
  }
  if (ioChannel == 1)
    pulseWidth1 = width;
  else if (ioChannel == 2)
    pulseWidth2 = width;

  return motSetMFFOperparams();
}

bool MFF::mffOperParamsPrepared() {
  if (mffOperParamsReady) return true;
  send(APT_MSG_ID::MOT_REQ_MFF_OPERPARAMS);
  std::unique_lock<std::mutex> lock(mffOperParamsMutex);
  mffOperParamsCV.wait(lock);
  mffOperParamsReady = true;
  return true;
}

uint32_t MFF::getITransitTime() {
  mffOperParamsPrepared();
  return iTransitTime;
}

uint32_t MFF::getITransitTimeADC() {
  mffOperParamsPrepared();
  return iTransitTimeADC;
}

uint16_t MFF::getOperMode1() {
  mffOperParamsPrepared();
  return operMode1;
}

uint16_t MFF::getSignalMode1() {
  mffOperParamsPrepared();
  return sigMode1;
}

uint32_t MFF::getPulseWidth1() {
  mffOperParamsPrepared();
  return pulseWidth1;
}

uint16_t MFF::getOperMode2() {
  mffOperParamsPrepared();
  return operMode2;
}

uint16_t MFF::getSignalMode2() {
  mffOperParamsPrepared();
  return sigMode2;
}
uint32_t MFF::getPulseWidth2() {
  mffOperParamsPrepared();
  return pulseWidth2;
}

std::string MFF::getPositionName() {
  getPosition();
  switch (pos) {
    case (position::down):
      return "Down";
    case (position::up):
      return "Up";
    case (position::motion):
      return "Motion";
    case (position::unknown1):
      return "Unknown1";
  };
  return "";
}
};  // namespace LIBAPT
