#include <getopt.h>
#include <string>
#include <cstring>

#include <apt.hpp>
using namespace LIBAPT;

char* id = nullptr;
MFF::position pos = MFF::position::unknown1;
bool scan = false;
bool get_pos = false;
bool set_pos = false;
bool verbose = false;
bool req_hw_info = false;
bool req_mff_params = false;
bool set_mff_param = false;
bool mod_identify = true;
unsigned long paramValue = 0;
std::string paramName = "";

void print_help()
{
  std::cout <<
    "--scan,      -s: Search for devices.\n"
    "--id,        -i: Set the serial number of the device.\n"
    "--identify,  -I: Flash the LED of the device.\n"
    "--hwinfo,    -w: Request hardware info.\n"
    "--mffparams, -m: Request hardware info.\n"
    "--set_pos,   -p: Set the position of the device. Possible values are up, down, toggle.\n"
    "--parameter, -P: Modify an MFF parameter. Possible values are ITransitTime, ITransitTimeADC.\n"
    "--value,     -V: Value of the parameter.\n"
    "--get_pos,   -g: Get the position of the device. Possible values are up, down.\n"
    "--verbose,   -v: Enable extra output.\n"
    "--help,      -h: Print this message.\n"
	    << std::endl;
}

void process_args(int argc, char* argv[])
{
  if (argc == 1) {
    scan = true;
  }

  const char* const short_opts = "i:p:gsvhwmP:V:I";
  const option long_opts[] = {
			      {"id",          required_argument, nullptr, 'i'},
			      {"set_pos",     required_argument, nullptr, 'p'},
			      {"get_pos",     no_argument,       nullptr, 'g'},
			      {"scan",        no_argument,       nullptr, 's'},
			      {"verbose",     no_argument,       nullptr, 'v'},
			      {"identify",    no_argument,       nullptr, 'I'},
			      {"req_hw_info", no_argument,       nullptr, 'w'},
			      {"mffparams",   no_argument,       nullptr, 'm'},
			      {"parameter",   required_argument, nullptr, 'P'},
			      {"value",       required_argument, nullptr, 'V'},
			      {"help",        no_argument,       nullptr, 'h'},
			      {nullptr,       no_argument,       nullptr,  0 }
  };

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt) {
    case 'i':
      id = optarg;
      break;

    case 'p':
      set_pos = true;
      if (!strcmp(optarg,"down"))
	pos = MFF::position::down;
      else if (!strcmp(optarg,"up"))
	pos = MFF::position::up;
      else if (!strcmp(optarg,"toggle"))
	pos = MFF::position::toggle;
      else
	std::cout << "Position not valid, aborting\n";
      break;

    case 's':
      scan = true;
      break;

    case 'g':
      get_pos = true;
      break;

    case 'v':
      verbose = true;
      break;

    case 'w':
      req_hw_info = true;
      break;

    case 'I':
      mod_identify = true;
      break;

    case 'P':
      set_mff_param = true;
      paramName = optarg;
      break;

    case 'V':
      paramValue = strtol(optarg, nullptr, 0);
      printf("%d, %.2x\n", paramValue, paramValue);
      break;

    case 'm':
      req_mff_params = true;
      break;

    case 'h': // -h or --help
    case '?': // Unrecognized option
    default:
      print_help();
      break;
    }
  }
}


int main(int argc, char* argv[])
{
  process_args(argc, argv);

  if (scan) {
    std::map<std::string, std::string> connectedDevices;
        device_scan(&connectedDevices);
        std::cout << "Number of devices found: " << connectedDevices.size() << "\n";
        for(const auto& [sn, dsc]: connectedDevices) {
            std::cout << "Type: " << sn
              << ". Serial Number: " << dsc << "\n";
        }
    std::exit(EXIT_SUCCESS);
  }

  MFF mff_device;
  bool init = mff_device.init(id, verbose);
  if (!init) {
    std::cout << mff_device.getError() << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if (mod_identify) {
    mff_device.send(APT_MSG_ID::MOD_IDENTIFY);
  }

  if (set_pos) {
    mff_device.motMoveJog(0, pos);
  }

  if (get_pos) {
    MFF::position gpos = mff_device.getPosition();
    std::cout << "Position: ";
    switch(gpos) {
    case MFF::position::down:
      std::cout << "down" << std::endl;
      break;
    case MFF::position::up:
      std::cout << "up" << std::endl;
      break;
    }
  }

  if(req_hw_info) {
    std::cout << "Serial Number: " << mff_device.getSerialNumber() << std::endl;
    std::cout << "Model Name: " << mff_device.getModelName() << std::endl;
    std::cout << "Firmware Version: " << mff_device.getFirmwareVersion() << std::endl;
    std::cout << "Hardware Version: " << mff_device.getHardwareVersion() << std::endl;
    std::cout << "Modification State: " << mff_device.getModificationState() << std::endl;
    std::cout << "Number of Channels: " << mff_device.getNumberChannels() << std::endl;
  }

  if (set_mff_param) {
    // bool set = mff_device.mot_set_mff_operparams(paramName, paramValue);
    // if (!set)
	// std::cout << mff_device.getError() << std::endl;
  }


  if(req_mff_params) {
    std::cout << "ITransitTime: " << mff_device.getITransitTime() << " ms " << std::endl;
    std::cout << "ITransitTimeADC: " << mff_device.getITransitTimeADC() << std::endl;
    std::cout << "OperMode1:   " << std::hex << static_cast<unsigned int>(mff_device.getOperMode1()) << std::dec << std::endl;
    std::cout << "SignalMode1: " << std::hex << static_cast<unsigned int>(mff_device.getSignalMode1()) << std::dec << std::endl;
    std::cout << "PulseWidth1: " << mff_device.getPulseWidth1() << " ms" << std::endl;
    std::cout << "OperMode2:   " << std::hex << static_cast<unsigned int>(mff_device.getOperMode2()) << std::dec << std::endl;
    std::cout << "SignalMode2: " << std::hex << static_cast<unsigned int>(mff_device.getSignalMode2()) << std::dec << std::endl;
    std::cout << "PulseWidth2: " << mff_device.getPulseWidth2() << " ms" << std::endl;
  }

  mff_device.finalize();
  std::exit(EXIT_SUCCESS);
}
