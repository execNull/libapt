#include <getopt.h>
#include <string>
#include <cstdint>

#include <apt.hpp>

using namespace LIBAPT;

char* id = nullptr;
int32_t pos = 0;
uint16_t channel = 0;
bool scan = false;
bool channel_set = false;
bool get_pos = false;
bool set_pos = false;
bool verbose = false;
bool req_hw_info = false;

void print_help()
{
  std::cout <<
    "--scan, -s: Search for devices.\n"
    "--id,-i: Set the serial number of the device.\n"
    "--hwinfo, -w: Request hardware info.\n"
    "--channel, -c: Set the channel for the operation\n"
    "--set_pos, -p: Set the position of the device. A channel must also be specified.\n"
    "--get_pos, -g: Get the position of all channels.\n"
    "--verbose, -v: Enable extra output.\n"
    "--help, -h: Print this message.\n"
	    << std::endl;
}

void process_args(int argc, char* argv[])
{
  if (argc == 1) {
    print_help();
    std::exit(EXIT_SUCCESS);
  }

  const char* const short_opts = "i:p:c:gsvwh";
  const option long_opts[] = {
    {"id",      required_argument, nullptr, 'i'},
    {"set_pos", required_argument, nullptr, 'p'},
    {"channel", required_argument, nullptr, 'c'},
    {"get_pos", no_argument,       nullptr, 'g'},
    {"scan",    no_argument,       nullptr, 's'},
    {"verbose", no_argument,       nullptr, 'v'},
    {"req_hw_info", no_argument,   nullptr, 'w'},
    {"help",    no_argument,       nullptr, 'h'},
    {nullptr,   no_argument,       nullptr,  0 }
  };

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt) {
    case 'i':
      id = optarg;
      break;

    case 'c':
      try {
	channel = std::stoi(optarg);
      }
      catch(std::invalid_argument &e) {
	std::cout << "Available channel values: 1 (x1), 2 (y1), 3 (x2), 4 (y2)." << std::endl;
	std::exit (EXIT_FAILURE);
      }
      if (channel < 1 | channel > 4){
	std::cout << "Available channel values: 1 (x1), 2 (y1), 3 (x2), 4 (y2)." << std::endl;
	std::exit(EXIT_FAILURE);
      }
	
      channel_set = true;
      break;

    case 'p':
      set_pos = true;
      pos = atoi(optarg);
      break;

    case 's':
      scan = true;
      break;

    case 'g':
      get_pos = true;
      break;

    case 'v':
      verbose = true;
      break;

    case 'w':
      req_hw_info = true;
      break;

    case 'h': // -h or --help
    case '?': // Unrecognized option
    default:
      print_help();
      break;
    }
  }

  if (set_pos && !channel_set) {
    std::cout << "To set the position, a channel is required. ";
    std::cout << "Please run again providing the -c <channel> option." << std::endl;
    std::exit(EXIT_FAILURE);
  }
}


int main(int argc, char* argv[])
{
  process_args(argc, argv);

  if (scan) {
    std::map<std::string, std::string> connectedDevices;
        aptDevScan(&connectedDevices);
        std::cout << "Number of devices found: " << connectedDevices.size() << "\n";
        for(const auto& [sn, dsc]: connectedDevices) {
            std::cout << "Type: " << sn
              << ". Serial Number: " << dsc << "\n";
        }
    std::exit(EXIT_SUCCESS);
  }

  KIM101 kim_device;
  bool init = kim_device.init(id, verbose);
  if (!init) {
    std::cout << kim_device.getError() << std::endl;
    return 1;
  }


  if (set_pos) {
    kim_device.pzmotMoveAbsolute(channel, pos);
    // std::cout << "sleep " << std::endl;
    // sleep(0.5);
    // kim_device.send(APT_MSG_ID::MOT_MOVE_STOP, {static_cast<uint8_t>(channel), static_cast<uint8_t>(0x1)});
  }

  if (get_pos)
    {
    std::vector<int32_t> gpos = kim_device.getPositions();
    std::cout << "X1 = " << gpos[0];
    std::cout << " Y1 = " << gpos[1];
    std::cout << " X2 = " << gpos[2];
    std::cout << " Y2 = " << gpos[3];
    std::cout << std::endl;
  }

  if(req_hw_info) {
    std::cout << "Serial Number: " << kim_device.getSerialNumber() << std::endl;
    std::cout << "Model Name: " << kim_device.getModelName() << std::endl;
    std::cout << "Model Type: " << kim_device.getHardwareType() << std::endl;
    std::cout << "Firmware Version: " << kim_device.getFirmwareVersion() << std::endl;
    std::cout << "Hardware Version: " << kim_device.getHardwareVersion() << std::endl;
    std::cout << "Mod State: " << kim_device.getModificationState() << std::endl;
    std::cout << "Number of Channels: " << kim_device.getNumberChannels() << std::endl;
  }

  kim_device.finalize();
  std::exit(EXIT_SUCCESS);
}
