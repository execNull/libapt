#include <getopt.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <ctime>

#include <cli/boostasioscheduler.h>
#include <cli/boostasioremotecli.h>
namespace cli
{
  using MainScheduler = BoostAsioScheduler;
  using CliTelnetServer = BoostAsioCliTelnetServer;
}
#include <cli/cli.h>
#include <cli/clilocalsession.h>
#include <cli/filehistorystorage.h>
using namespace cli;

// Headers for libapt library
#include "../include/apt.hpp"
using namespace LIBAPT;

//Headers for json
#include <nlohmann/json.hpp>
using json = nlohmann::json;

//C++17 filesystem
#include <filesystem>
namespace fs = std::filesystem;

//Default values
bool daemonize = false;
int port = 5000;

std::string getDateTime()
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
    return ss.str();
}

struct ConnectedType {
  virtual ~ConnectedType() {};
  std::string name;
  std::string type = "";
  bool lock = false;
  struct {
    std::shared_ptr<APTDev> dev;
    std::string serialNumber;
    APTDevType type;
    uint8_t channel;
  } connection;
  std::string position = "----";
  APTDevStatus status = APTDevStatus::NotFound;

  virtual void init(std::ostream& out) = 0;
  virtual void finalize() {
    if (connection.dev == nullptr) return;
    connection.dev->finalize();
  };
  void identify(std::ostream& out) {
    if (connection.dev == nullptr) return;
    bool status = connection.dev->send(APT_MSG_ID::MOD_IDENTIFY);
    if(!status)
      out<< connection.dev->getError() << "\n";
  }

  virtual void rebase(std::ostream& out) = 0;
  virtual void moveHome(std::ostream& out) = 0;
  virtual void updatePosition() = 0;
  virtual void setPositionAbsolute(std::ostream& out, std::string pos) = 0;
  virtual void setPositionRelative(std::ostream& out, std::string pos) = 0;
  void updateStatus() {
    if (connection.dev == nullptr) {
      status = APTDevStatus::NotFound;
      return;
    }
    status = connection.dev->getStatus();
  }
};

struct Translator : public ConnectedType {
  Translator() { type = "Translator";}
  void init(std::ostream& out) override {
    if (connection.dev == nullptr) {
      return;
    }
    auto kdc = std::static_pointer_cast<KDC101>(connection.dev);
    if (!kdc->init()) {
      out << kdc->getError() << "\n";
    }
  }
  void updatePosition() override {
    if (connection.dev == nullptr) {
      position = "Unknown";
      return;
    }
    auto kdc = std::static_pointer_cast<KDC101>(connection.dev);
    position = std::to_string(kdc->getPosition());
  };

  void setPositionAbsolute(std::ostream& out, std::string pos) override {
    if (connection.dev == nullptr) {
      return;
    }
    bool position_set = false;
    int32_t p = static_cast<int32_t>(std::stof(pos));
    auto kdc = std::static_pointer_cast<KDC101>(connection.dev);
    position_set = kdc->motMoveAbsolute(connection.channel, p);
    if (!position_set)
      out << kdc->getError() << "\n";
    else
      position = pos;
  }

  void setPositionRelative(std::ostream& out, std::string pos) override {
    if (connection.dev == nullptr) {
      return;
    }
    bool position_set = false;
    double p = static_cast<double>(std::stof(pos));
    auto kdc = std::static_pointer_cast<KDC101>(connection.dev);
    position_set = kdc->motMoveRelative(connection.channel, p);
    if (!position_set)
      out << kdc->getError() << "\n";
    else
      position = pos;
  }

  void moveHome(std::ostream& out) override {
    auto kdc = std::static_pointer_cast<KDC101>(connection.dev);
    if (!kdc->motMoveHome(connection.channel)) {
      out << kdc->getError() << "\n";
    }
    else
      updatePosition();
  }

  void rebase(std::ostream& out) override {
    out << "Shutters cannot be rebased.\n";
  }
};

struct Shutter : public ConnectedType {
  Shutter() {type = "Shutter";};
  void init(std::ostream& out) override {
    if (connection.dev == nullptr) {
      return;
    }
    auto mff = std::static_pointer_cast<MFF>(connection.dev);
    if (!mff->init()) {
      out << mff->getError() << "\n";
    }
  }
  void updatePosition() override {
    if (connection.dev == nullptr) {
      position = "Unknown";
      return;
    }
    auto mff = std::static_pointer_cast<MFF>(connection.dev);
    position = mff->getPositionName();
  }
  void setPositionAbsolute(std::ostream& out, std::string pos) override {
    bool position_set = false;
    if (connection.type == APTDevType::MFF) {
      auto mff = std::static_pointer_cast<MFF>(connection.dev);
      MFF::position posString = MFF::position::unknown1;
      if (pos.compare("down") == 0) {
	posString = MFF::position::down;
      } else if (pos.compare("up") == 0)
	posString = MFF::position::up;
      else if (pos.compare("toggle") == 0)
	posString = MFF::position::toggle;
      else {
	out << "Position can be up, down, or toggle\n";
	return;
      }
      position_set = mff->motMoveJog(connection.channel, posString);
      if (!position_set)
	out << mff->getError() << "\n";
      else
	position = pos;
    }
  }

  void setPositionRelative(std::ostream& out, std::string pos) override {
    setPositionAbsolute(out, pos);
  };

  void moveHome(std::ostream& out) override {
    out << "Shutters do not have a home position.\n";
  };

  void rebase(std::ostream& out) override {
    out << "Shutters cannot be rebased.\n";
  };
};

struct Actuator : public ConnectedType {
  Actuator() {type = "Actuator";};

  void init(std::ostream& out) override {
    if (connection.dev == nullptr) {
      return;
    }
    auto kim = std::static_pointer_cast<KIM101>(connection.dev);
    if (!kim->init()) {
      out << kim->getError() << "\n";
    }
  }
  void updatePosition() override {
    if (connection.dev == nullptr) {
      position = "Unknown";
      return;
    }
    auto kim = std::static_pointer_cast<KIM101>(connection.dev);
    position = std::to_string(kim->getPosition(connection.channel));
  }
  void setPositionAbsolute(std::ostream& out, std::string pos) override {
    if (connection.dev == nullptr) {
      return;
    }
    bool position_set = false;
    int32_t p = static_cast<int32_t>(std::stol(pos));
    auto kim = std::static_pointer_cast<KIM101>(connection.dev);
    position_set = kim->pzmotMoveAbsolute(connection.channel, p);
    if (!position_set)
      out << kim->getError() << "\n";
    else
      position = pos;
  }

  void setPositionRelative(std::ostream& out, std::string pos) override {
    if (connection.dev == nullptr) {
      return;
    }
    bool position_set = false;
    auto kim = std::static_pointer_cast<KIM101>(connection.dev);
    updatePosition();
    int32_t p1 = static_cast<int32_t>(std::stol(pos));
    int32_t p2 = static_cast<int32_t>(std::stol(position));
    int32_t new_position = p1 + p2;
    position_set = kim->pzmotMoveAbsolute(connection.channel, new_position);
    if (!position_set)
      out << kim->getError() << "\n";
    else {
    position = std::to_string(new_position);
    }
  };

  void moveHome(std::ostream& out) override {
    out << "Actuators do not have a home position.\n";
  };

  void rebase(std::ostream& out) override {
    auto kim = std::static_pointer_cast<KIM101>(connection.dev);
    if (!kim->posCounts(connection.channel, 0))
      out << kim->getError() << "\n";
  }
};

void print_help(std::ostream& out) {
  out << "--daemonize, -d: Daemonize the process.\n"
    "--port,      -p: Port to use for the telnet interface.\n"
    "--help,      -h: Print this message.\n"
    ;
}

void process_args(int argc, char* argv[]) {
  const char* const short_opts = "dhp:";
  const option long_opts[] = {
    {"daemonize",   no_argument,       nullptr, 'd'},
    {"port",        required_argument, nullptr, 'p'},
    {"help",        no_argument,       nullptr, 'h'},
    {nullptr,       no_argument,       nullptr,  0 }
  };

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt) {
    case 'd':
      daemonize = true;
      break;

    case 'p':
      port = atoi(optarg);
      break;

    case 'h': // -h or --help
    case '?': // Unrecognized option
    default:
      print_help(std::cout);
      std::exit(EXIT_SUCCESS);
      break;
    }
  }
}

/**
 * Return an environmental variable based on its name
 **/
std::string getEnvVar(std::string const& key)
{
  char const* val = getenv(key.c_str());
  return val == nullptr ? std::string() : std::string(val);
}

int main(int argc, char* argv[]) {
  process_args(argc, argv);

  fs::path configFile; ///Absolute path of the configuration file

  /** Function trying to find the configuration file.
   *  The first path checked is the location of the executable,
   *  then, XDG_CONFIG_HOME, then $HOME/.config, then /etc.
   **/
  auto setConfig = [&configFile](std::ostream& out) {
    fs::path configFileName("apt_cli.conf");
    fs::path xdgConfigHome = getEnvVar("XDG_CONFIG_HOME");
    fs::path homePath = getEnvVar("HOME");

    //Check the current path
    fs::path p (fs::current_path()/configFileName);

    if (fs::exists(p)) {
      configFile = p;
      return;
    }
    //First check if a user config file exists
    if (!xdgConfigHome.empty()) {
      p = xdgConfigHome/configFileName;
      if (fs::exists(p)) {
	configFile = p;
	return;
      }
    }
    else {
      p = homePath/".config"/configFileName;
      if (fs::exists(p)) {
	configFile = p;
	return;
      }
    }
  };

  ///Factory for the generation of APT_Devices
  APTDevFactory aptDevFactory;
  ///The devices that have been used since the execution of the program.
  std::map<std::string, std::shared_ptr<APTDev>> deviceMap;
  ///List of actuators loaded from the configuration file.
  std::map<std::string, std::unique_ptr<ConnectedType>> actuators;
  std::string activeDevice = "";

  auto associate = [&deviceMap, &actuators](std::ostream& out) {
    for (const auto& [name, act]: actuators) {
      if (deviceMap.empty()) {
	(*act).connection.dev = nullptr;
      }
      else if (deviceMap.find((*act).connection.serialNumber) != deviceMap.end()) {
	(*act).connection.dev = (deviceMap[(*act).connection.serialNumber]);
      }
      (*act).updateStatus();
    }
  };

  ///Populate the deviceMap
  auto scan = [&aptDevFactory, &associate, &deviceMap](std::ostream& out) {
    //The devices which can be found connected to the computer running the server.
    std::map<std::string, APTDevType> connectedDevices;
    aptDevScan(&connectedDevices);

    //Add the devices found to the deviceMap
    for (const auto& [sn, devtype]: connectedDevices) {
      //If the device is already in the list, move on
      if (deviceMap.find(sn) != deviceMap.end()) {
	continue;
      }

      //else, create a new device and add it on the list
      auto dev = aptDevFactory.create(devtype, sn);
      deviceMap.emplace(sn, std::move(dev));
    }

    //In case some devices have been powered off, deviceMap has to
    //remove them as well.
    if (deviceMap.size() != connectedDevices.size())  {
      for (auto it = deviceMap.begin(); it != deviceMap.end();) {
	if (connectedDevices.find(it->first) == connectedDevices.end())
	  it = deviceMap.erase(it);
	else
	  it++;
      }
    }

    //Associate the devices to their actuators
    associate(std::cout);

    //Print the results
    out << "----- Number of devices found: " << deviceMap.size() << " -----\n";
    out << std::setw(15) << std::left << "Serial Number";
    out << std::setw(15) << std::left << "Type";
    out << "\n";
    for(const auto& [sn, dev]: deviceMap) {
      out << std::setw(15) << std::left << sn;
      out << std::setw(15) << std::left << dev->getType();
      out << "\n";
    }
    out << "----------------------------------------------\n";
  };

  ///Reload  the configuration file, clearing and refilling the actuators map.
  auto reload_config = [&configFile, &actuators](std::ostream& out) {
    if (!fs::exists(configFile)) {
      out << "Configuration file not found: " << configFile << ".\n";
      return;
    }
    else
      out << configFile << "\n";

    std::ifstream c(configFile);
    json j = json::parse(c, nullptr, false);
    if (j.is_discarded()) {
      out << "Error parsing configuration file. Aborting.\n";
      return;
    }
    actuators.clear();
    // try {

    for (const auto& dev: j["actuators"]) {
      json con = dev.at("connection");
      if (con.at("type") == "MFF") {
	std::unique_ptr<Shutter> s(new Shutter());
	actuators.emplace(dev["name"], std::move(s));
	(*actuators[dev["name"]]).connection.type = APTDevType::MFF;
      }
      else if (con.at("type") == "KIM101") {
	std::unique_ptr<Actuator> a (new Actuator());
	actuators.emplace(dev["name"], std::move(a));
	(*actuators[dev["name"]]).connection.type = APTDevType::KIM101;
      }
      else if (con.at("type") == "KDC101") {
	std::unique_ptr<Translator> a (new Translator());
	actuators.emplace(dev["name"], std::move(a));
	(*actuators[dev["name"]]).connection.type = APTDevType::KDC101;
      }
      if (actuators[dev["name"]] == nullptr)
	continue;
      (*actuators[dev["name"]]).name = dev["name"];
      (*actuators[dev["name"]]).lock = dev["lock"];
      (*actuators[dev["name"]]).connection.channel = con.at("channel");
      (*actuators[dev["name"]]).connection.serialNumber = con.at("serialNumber");

      (*actuators[dev["name"]]).updatePosition();
      (*actuators[dev["name"]]).updateStatus();
    }
  };

  auto list_actuators = [&scan, &associate, &actuators](std::ostream& out){
    // std::cout.setstate(std::ios_base::badbit);
    scan(out);
    // std::cout.clear();
    out << std::setw(15) << std::left << "Name";
    out << std::setw(15) << std::left << "Type";
    out << std::setw(15) << std::left << "Position";
    out << std::setw(15) << std::left << "Status";
    out << std::setw(7)  << std::left << "Locked";
    out << std::setw(10) << std::left << "Type";
    out << std::setw(10) << std::left << "Channel";
    out << std::setw(15) << std::left<< "SerialNumber";
    out << "\n";
    for (const auto& [n, dev]: actuators) {
      (*dev).updateStatus();
      (*dev).updatePosition();
      out << std::setw(15) << std::left << (*dev).name;
      out << std::setw(15) << std::left << (*dev).type;
      out << std::setw(15) << std::left << (*dev).position;
      out << std::setw(15) << std::left << (*dev).status;
      out << std::setw(7)  << std::left << (*dev).lock;
      out << std::setw(15) << std::left << (*dev).connection.type;
      out << std::setw(10) << std::left << std::to_string((*dev).connection.channel);
      out << std::setw(15) << std::left<< (*dev).connection.serialNumber;
      out << "\n";
    }
  };

  auto act_init = [&actuators](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device not found, aborting.\n";
      return;
    }
    (*actuators[name]).init(out);
  };

  auto act_get_pos = [&act_init, &actuators](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).updatePosition();
    out << (*actuators[name]).position << "\n";
  };

  auto act_set_pos_abs = [&act_init, &actuators](std::ostream& out, std::string name, std::string pos) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    if ((*actuators[name]).lock) {
      out << "Device " << name << " is locked, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).setPositionAbsolute(out, pos);
    fs::path ph = getEnvVar("HOME");
    fs::path pa = ".apt_cli";
    fs::path pn = (name + ".log");
    fs::path p = ph / pa /pn;
    std::ofstream f(p,std::ios_base::app | std::ios_base::out );
    f << getDateTime() << " absolute " << pos << std::endl;
    f.close();
  };

  auto act_set_pos_rel = [&act_init, &actuators](std::ostream& out, std::string name, std::string pos) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    if ((*actuators[name]).lock) {
      out << "Device " << name << " is locked, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).setPositionRelative(out, pos);
    fs::path ph = getEnvVar("HOME");
    fs::path pa = ".apt_cli";
    fs::path pn = (name + ".log");
    fs::path p = ph / pa /pn;
    std::ofstream f(p,std::ios_base::app | std::ios_base::out );
    f << getDateTime() << " relative " << (*actuators[name]).position << "\n";
    f.close();
  };

  auto act_finalize = [&actuators](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    (*actuators[name]).finalize();
  };

  auto act_finalize_all = [&act_finalize, &actuators](std::ostream& out) {
    for (const auto& [name, act]: actuators) {
      act_finalize(out, name);
    }
  };

  ///Lock a channel so its position cannot be changed
  auto act_lock = [&actuators](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    (*actuators[name]).lock = true;
  };

  ///Lock a channel so its position cannot be changed
  auto act_unlock = [&actuators](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    (*actuators[name]).lock = false;
  };

  auto act_rebase = [&actuators, &act_init](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).rebase(out);
    fs::path ph = getEnvVar("HOME");
    fs::path pa = ".apt_cli";
    fs::path pn = (name + ".log");
    fs::path p = ph / pa /pn;
    std::ofstream f(p,std::ios_base::app | std::ios_base::out);
    f << getDateTime() << " rebase   " << 0 << std::endl;
    f.close();
  };

  auto act_identify = [&actuators, &act_init](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).identify(out);

  };

  auto act_move_home = [&actuators, &act_init](std::ostream& out, std::string name) {
    if (actuators.find(name) == actuators.end()) {
      out << "Device " << name << " not found, aborting.\n";
      return;
    }
    act_init(out, name);
    (*actuators[name]).moveHome(out);
  };

  auto dev_enable = [&activeDevice, &deviceMap](std::ostream& out, std::string sn) {
    if (deviceMap.find(sn) == deviceMap.end()) {
      out << "Device " << sn << " not found, aborting.\n";
      return;
    }
    activeDevice = sn;
  };

  auto dev_init = [&activeDevice, &deviceMap](std::ostream& out) {
    if (deviceMap.find(activeDevice) == deviceMap.end()) {
      out << "Device " << activeDevice << " not found, aborting.\n";
      return;
    }
    deviceMap[activeDevice]->init();
  };

  auto dev_getHWInfo = [&activeDevice, &deviceMap](std::ostream& out) {
    out << "Serial number: "      << deviceMap[activeDevice]->getSerialNumber()      << "\n";
    out << "Model name: "         << deviceMap[activeDevice]->getModelName()         << "\n";
    out << "Hardware type: "      << deviceMap[activeDevice]->getHardwareType()      << "\n";
    out << "Firmware version: "   << deviceMap[activeDevice]->getFirmwareVersion()   << "\n";
    out << "Hardware version: "   << deviceMap[activeDevice]->getHardwareVersion()   << "\n";
    out << "Modification state: " << deviceMap[activeDevice]->getModificationState() << "\n";
    out << "Number of Channels: " << deviceMap[activeDevice]->getNumberChannels()    << "\n";
  };

  auto dev_get_pos = [&aptDevFactory, &activeDevice, &deviceMap](std::ostream& out) {
    auto dev = std::dynamic_pointer_cast<KDC101>(deviceMap[activeDevice]);
    // out << dev->getType() << "\n";
    // if (!dev->getPosition(0))
    //   std::cout << dev->getError() << std::endl;
    // else
      out << dev->getPosition() << "\n";
  };

  auto dev_set_pos_rel = [&aptDevFactory, &activeDevice, &deviceMap](std::ostream& out, double position) {
    auto dev = std::dynamic_pointer_cast<KDC101>(deviceMap[activeDevice]);
    if (!dev->motMoveRelative(0, position))
      out << dev->getError() << "\n";
        fs::path ph = getEnvVar("HOME");

  };

  auto dev_set_pos_abs = [&aptDevFactory, &activeDevice, &deviceMap](std::ostream& out, double position) {
    auto dev = std::dynamic_pointer_cast<KDC101>(deviceMap[activeDevice]);
    if (!dev->motMoveAbsolute(0, position))
      out << dev->getError() << "\n";
  };

  auto dev_move_home = [&aptDevFactory, &activeDevice, &deviceMap](std::ostream& out) {
    auto dev = std::dynamic_pointer_cast<KDC101>(deviceMap[activeDevice]);
    if (!dev->motMoveHome(0))
      out << dev->getError() << "\n";
  };

  auto dev_finalize = [&activeDevice, &deviceMap](std::ostream& out) {
    auto dev = std::dynamic_pointer_cast<KDC101>(deviceMap[activeDevice]);
    if (dev->getStatus() == APTDevStatus::Initialized) {
      if (dev->finalize()) {
	out << "Device finalized\n";
	return;
      }
      else
	out << dev->getError() << "\n";
    }
    out << "Device already finalized\n";
  };

  ///Get the position of an mff device.
  auto mff_get_pos = [&activeDevice, &deviceMap](std::ostream& out) {
    auto dev = std::dynamic_pointer_cast<MFF>(deviceMap[activeDevice]);
    if (dev == nullptr) {
      out << "No enabled device found, please use enable first.\n";
      return;
    }
    out << "Position: " << dev->getPositionName() << "\n";
  };

  ///Set the position of an MFF device. The positions can be up, down or toggle.
  auto mff_set_pos = [&deviceMap, &activeDevice](std::ostream& out, std::string p) {
    auto mff = std::dynamic_pointer_cast<MFF>(deviceMap[activeDevice]);
    if (mff == nullptr) {
      out << "No enabled device found, please use enable first.\n";
      return;
    }
    MFF::position pos = MFF::position::unknown1;
    if (p.compare("down") == 0) {
      pos = MFF::position::down;
    } else if (p.compare("up") == 0)
      pos = MFF::position::up;
    else if (p.compare("toggle") == 0)
      pos = MFF::position::toggle;
    else {
      std::cout << "Position can be up, down, or toggle\n";
      return;
    }
    if (!mff->motMoveJog(0, pos))
      out << mff->getError() << "\n";
  };

  ///Get the position of an KIM101 channel.
  auto kim_get_pos = [&deviceMap, &activeDevice](std::ostream& out, uint16_t channel) -> int32_t {
    auto kim = std::dynamic_pointer_cast<KIM101>(deviceMap[activeDevice]);
    if (kim == nullptr) {
      out << "No enabled device found, please use enable first.\n";
      return -1;
    }
    out <<  kim->getPosition(channel) << "\n";
  };

  ///Set the position of an KIM101 channel.
  auto kim_set_pos = [&deviceMap, &activeDevice](std::ostream& out, uint16_t channel, int32_t pos ) {
    auto kim = std::dynamic_pointer_cast<KIM101>(deviceMap[activeDevice]);
    if (kim == nullptr) {
      out << "No enabled device found, please use enable first.\n";
      return;
    }
    if (!kim->pzmotMoveAbsolute(channel, pos))
      out << kim->getError() << "\n";
  };

  ///Set the current position of a KIM101 channel to zero.
  auto kim_rebase = [&deviceMap, &activeDevice](std::ostream& out, uint16_t channel) -> uint32_t {
    auto kim = std::dynamic_pointer_cast<KIM101>(deviceMap[activeDevice]);
    if (kim == nullptr) {
      out << "No enabled device found, please use enable first.\n";
      return -1;
    }
    if (!kim->posCounts(channel, 0))
      out << kim->getError() << "\n";
  };

  auto print_history = [](std::ostream& out, std::string name) {
    fs::path homePath = getEnvVar("HOME");
    fs::path p = homePath / ".apt_cli" / (name + ".log");
    std::ifstream file(p);
    std::string str;
    while (std::getline(file, str)) {
      out << str << "\n";
    }
    file.close();
  };

  auto rootMenu = std::make_unique<Menu>("cli");
  rootMenu->Insert("scan", scan, "Scan for connected devices.");
  rootMenu->Insert("reload_config", reload_config, "Reload the configuration file");
  rootMenu->Insert("list_actuators", list_actuators, "Print the actutor list");
  rootMenu->Insert("init", act_init, "Initialize an actuator");
  rootMenu->Insert("finalize", act_finalize, "Finalize an actuator");
  rootMenu->Insert("finalize_all", act_finalize_all, "Finalize all actuators");
  rootMenu->Insert("get_pos", act_get_pos, "Get the position of an actuator");
  rootMenu->Insert("set_pos", act_set_pos_abs, "Set the absolute position of an actuator");
  rootMenu->Insert("set_pos_rel", act_set_pos_rel, "Set the relative position of an actuator");
  rootMenu->Insert("lock", act_lock, "Lock an actuator");
  rootMenu->Insert("unlock", act_unlock, "Lock an actuator");
  rootMenu->Insert("home", act_move_home, "Send a translator to its home position");
  rootMenu->Insert("tr50", [&act_set_pos_abs](std::ostream&out, std::string name) {
    act_set_pos_abs(out, name, "50");
  }, "Send a translator to position 50");
  rootMenu->Insert("tr0", [&act_set_pos_abs](std::ostream&out, std::string name) {
    act_set_pos_abs(out, name, "0");
  }, "Send a translator to position 0");
  rootMenu->Insert("rebase", act_rebase, "Rebase an actuator");
  rootMenu->Insert("identify", act_identify, "Flash the led of the device controlling the specified actuator");
  rootMenu->Insert("print_history", print_history, "Print the history of an actuator");

  auto mffMenu = std::make_unique<Menu>("mff");
  mffMenu->Insert("enable", dev_enable, "Set the main device.");
  mffMenu->Insert("init", dev_init, "Initialize the device.");
  mffMenu->Insert("finalize", dev_finalize, "Finalize the active device");
  // mffMenu->Insert("identify", dev_identify, "Flash the led of the device");
  mffMenu->Insert("get_pos", mff_get_pos, "Get the position of the device");
  mffMenu->Insert("set_pos", mff_set_pos, "Set the position of the device");
  // mffMenu->Insert("req_hw_info", req_hw_info, "Request the hardware information for the device");
  // mffMenu->Insert("verbose", verbose, "Activate or deactivate the verbosity of the device");
  // mffMenu->Insert("status", mff_status, "Display the status of all connected MFF devices");

  auto kimMenu = std::make_unique<Menu>("kim");
  kimMenu->Insert("enable", dev_enable, "Set the main device.");
  kimMenu->Insert("init",   dev_init,     "Initialize the active device.");
  kimMenu->Insert("finalize", dev_finalize, "Finalize a device.");
  // kimMenu->Insert("identify", dev_identify, "Flash the led on the device.");
  kimMenu->Insert("get_pos", kim_get_pos, "Print all positions for the device.");
  kimMenu->Insert("set_pos", kim_set_pos, "Set a potision for a channel.");
  // kimMenu->Insert("req_hw_info", req_hw_info, "Display the hardware information of the device.");
  // kimMenu->Insert("verbose", verbose, "activate or deactivate the verbosity of a device.");
  kimMenu->Insert("rebase", kim_rebase, "Set the homing position for a channel");
  // kimMenu->Insert("status", kim_status, "Display the status of connected KIM101 devices");

  auto kdcMenu = std::make_unique<Menu>("kdc");
  kdcMenu->Insert("enable", dev_enable, "Set the main device.");
  kdcMenu->Insert("init", dev_init, "Initialize the device.");
  kdcMenu->Insert("get_info", dev_getHWInfo, "Print the info for the device.");
  kdcMenu->Insert("finalize", dev_finalize, "Finalize the device.");
  kdcMenu->Insert("get_pos", dev_get_pos, "Finalize the device.");
  kdcMenu->Insert("set_pos_rel", dev_set_pos_rel, "Finalize the device.");
  kdcMenu->Insert("set_pos_abs", dev_set_pos_abs, "Finalize the device.");
  kdcMenu->Insert("move_home", dev_move_home, "Move to the home position.");

  rootMenu->Insert(std::move(kimMenu));
  rootMenu->Insert(std::move(mffMenu));
  rootMenu->Insert(std::move(kdcMenu));

  Cli cli(std::move(rootMenu), std::make_unique<FileHistoryStorage>(".cli"));
  cli.ExitAction([](auto& out) { out << "Exiting.\n"; });
  cli.StdExceptionHandler([](std::ostream& out, const std::string& cmd, const std::exception& e) {
			    out << "Exception caught in cli handler: " << e.what()
				<< " handling command: " << cmd << ".\n";
			  });

  MainScheduler scheduler;
  if(!daemonize) {
    std::cout << "Welcome to APT Command Line Interface. Type help for, well, help.\n";
    setConfig(std::cout);
    reload_config(std::cout);
    scan(std::cout);
    CliLocalTerminalSession localSession(cli, scheduler, std::cout, 200);
    localSession.ExitAction([&scheduler](auto& out) {
      out << "Closing App...\n";
      scheduler.Stop();
    });
    scheduler.Run();
  }

  if(daemonize) {
    syslog (LOG_NOTICE, "apt_cli daemon started.");
    CliTelnetServer server(cli, scheduler, port );
    setConfig(std::cout);
    reload_config(std::cout);
    server.ExitAction([](auto& out) { out << "Terminating this session...\n";});
    scheduler.Run();
    syslog (LOG_NOTICE, "apt_cli daemon exited.");
    closelog();
  }

  act_finalize_all(std::cout);
  return 0;
}
