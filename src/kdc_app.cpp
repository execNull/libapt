#include <getopt.h>
#include <string>
#include <iomanip>

#include <apt.hpp>
using namespace LIBAPT;

char* id = nullptr;
bool scan = false;
bool verbose = false;
bool req_hw_info = false;
bool mod_identify = false;
bool moveAbsolute = false;
bool moveRelative = false;
bool moveHome = false;
bool getPos = false;

double absPosMM = 0;
double relPosMM = 0;

void print_help()
{
  std::cout <<
    "--scan,      -s: Search for devices.\n"
    "--id,        -i: Set the serial number of the device.\n"
    "--identify,  -I: Flash the LED of the device.\n"
    "--hwinfo,    -w: Request hardware info.\n"
    "--absmove,   -a: Move to absolute position (in mm).\n"
    "--relmove,   -r: Move to relative position (in mm).\n"
    "--home,      -H: Move to home position.\n"
    "--pos,       -p: Get the current position (in mm).\n"
    "--help,      -h: Print this message.\n"
	    << std::endl;
}

void process_args(int argc, char* argv[])
{
  if (argc == 1) {
    scan = true;
  }

  const char* const short_opts = "i:svIwha:r:Hp";
  const option long_opts[] = {
    {"id",          required_argument, nullptr, 'i'},
    {"scan",        no_argument,       nullptr, 's'},
    {"verbose",     no_argument,       nullptr, 'v'},
    {"identify",    no_argument,       nullptr, 'I'},
    {"req_hw_info", no_argument,       nullptr, 'w'},
    {"help",        no_argument,       nullptr, 'h'},
    {"absmove",     required_argument, nullptr, 'a'},
    {"relmove",     required_argument, nullptr, 'r'},
    {"moveHome",    no_argument,       nullptr, 'H'},
    {"pos",         no_argument,       nullptr, 'p'},
    {nullptr,       no_argument,       nullptr,  0 }
  };

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt) {
    case 'i':
      id = optarg;
      break;

    case 's':
      scan = true;
      break;

    case 'v':
      verbose = true;
      break;

    case 'w':
      req_hw_info = true;
      break;

    case 'I':
      mod_identify = true;
      break;

    case 'a':
      moveAbsolute = true;
      absPosMM = std::stod(optarg);
      break;

    case 'r':
      moveRelative = true;
      relPosMM = std::stod(optarg);
      break;

    case 'H':
      moveHome = true;
      break;

    case 'p':
      getPos = true;
      break;

    case 'h': // -h or --help
    case '?': // Unrecognized option
    default:
      print_help();
      break;
    }
  }
}


int main(int argc, char* argv[])
{
  process_args(argc, argv);

  if (scan) {
    std::map<std::string, APTDevType> connectedDevices;
    aptDevScan(&connectedDevices);
    //Print the results
    std::cout << "----- Number of devices found: " << connectedDevices.size() << " -----\n";
    std::cout << std::setw(15) << std::left << "Serial Number";
    std::cout << std::setw(15) << std::left << "Type";
    std::cout << "\n";
    for(const auto& [sn, dev]: connectedDevices) {
      std::cout << std::setw(15) << std::left << sn;
      std::cout << std::setw(15) << std::left << dev;
      std::cout << "\n";
    }
    std::cout << "----------------------------------------------\n";
  }

  KDC101 kdc_device(id);
  bool init = kdc_device.init();
  if (!init) {
    std::cout << kdc_device.getError() << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if (mod_identify) {
    kdc_device.send(APT_MSG_ID::MOD_IDENTIFY);
  }
  if(req_hw_info) {
    std::cout << "Serial Number: " << kdc_device.getSerialNumber() << std::endl;
    std::cout << "Model Name: " << kdc_device.getModelName() << std::endl;
    std::cout << "Model Type: " << kdc_device.getHardwareType() << std::endl;
    std::cout << "Firmware Version: " << kdc_device.getFirmwareVersion() << std::endl;
    std::cout << "Hardware Version: " << kdc_device.getHardwareVersion() << std::endl;
    std::cout << "Mod State: " << kdc_device.getModificationState() << std::endl;
    std::cout << "Number of Channels: " << kdc_device.getNumberChannels() << std::endl;
  }

  if (moveAbsolute) {
    kdc_device.motMoveAbsolute(0, absPosMM);
  }

  if (moveRelative) {
    kdc_device.motMoveRelative(0, relPosMM);
  }

  if (moveHome) {
    kdc_device.motMoveHome();
  }

  if (getPos) {
    std::cout << std::setprecision(4);
    std::cout << "Current position: " << kdc_device.getPosition() << std::endl;
  }

  kdc_device.finalize();
  std::exit(EXIT_SUCCESS);
}
