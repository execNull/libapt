#include <apt.hpp>
#include <apt_msg.hpp>

namespace LIBAPT {
KIM101::KIM101(std::string sn) {
  if (serialNumber.empty()) {
    serialNumber = sn;
  }
  devType = APTDevType::KIM101;
  addMsg(APT_MSG_ID::MOD_SET_CHANENABLESTATE);
  addMsg(APT_MSG_ID::MOD_REQ_CHANENABLESTATE);
  addMsg(APT_MSG_ID::MOD_GET_CHANENABLESTATE);
  addMsg(APT_MSG_ID::HW_DISCONNECT);
  addMsg(APT_MSG_ID::HW_RESPONSE);
  addMsg(APT_MSG_ID::HW_RICHRESPONSE, 74);
  addMsg(APT_MSG_ID::HUB_REQ_BAYUSED);
  addMsg(APT_MSG_ID::HUB_GET_BAYUSED);
  addMsg(APT_MSG_ID::MOT_MOVE_STOP);
  addMsg(APT_MSG_ID::MOT_SET_EEPROMPARAMS);
  addMsg(APT_MSG_ID::MOT_GET_STATUSUPDATE);
  addMsg(APT_MSG_ID::PZMOT_SET_PARAMS);
  addMsg(APT_MSG_ID::PZMOT_REQ_PARAMS);
  addMsg(APT_MSG_ID::PZMOT_GET_PARAMS);
  addMsg(APT_MSG_ID::PZMOT_MOVE_ABSOLUTE, 12);
  addMsg(APT_MSG_ID::PZMOT_MOVE_COMPLETED, 20);
  addMsg(APT_MSG_ID::PZMOT_MOVE_JOG);
  addMsg(APT_MSG_ID::PZMOT_REQ_STATUSUPDATE);
  addMsg(APT_MSG_ID::PZMOT_GET_STATUSUPDATE, 62);

  pzmotMoveCompleted = [this](APT_MSG& msg) { moveCompletedCV.notify_all(); };
  addMsgAction(APT_MSG_ID::PZMOT_MOVE_COMPLETED, pzmotMoveCompleted);

  pzmotGetStatusUpdate = [this](APT_MSG& msg) {
    msg.extractValueFromBytes(8, 11, x1);
    msg.extractValueFromBytes(22, 25, y1);
    msg.extractValueFromBytes(36, 38, x2);
    msg.extractValueFromBytes(50, 53, y2);
    moveCV.notify_all();
  };
  addMsgAction(APT_MSG_ID::PZMOT_GET_STATUSUPDATE, pzmotGetStatusUpdate);
}

auto KIM101::init() -> bool {
  if (!APTDev::init()) {
    return false;
  }
  // send(LIBAPT::APT_MSG_ID::HW_START_UPDATEMSGS);
  send(LIBAPT::APT_MSG_ID::HW_REQ_INFO);
  send(LIBAPT::APT_MSG_ID::PZMOT_REQ_STATUSUPDATE);
  return true;
}

int32_t KIM101::getPosition(uint16_t channel) {
  if (getStatus() == APTDevStatus::Initialized) {
 
    send(LIBAPT::APT_MSG_ID::PZMOT_REQ_STATUSUPDATE);
    std::unique_lock<std::mutex> lock(moveMutex);
    if (moveCV.wait_for(lock, std::chrono::seconds(5)) == std::cv_status::timeout) {
	return 5;
    }
  }

  int32_t pos = 0;
  if (channel == 1) {
      pos = x1;
  } else if (channel == 2) {
      pos = y1;
  } else if (channel == 3) {
      pos = x2;
  } else if (channel == 4) {
      pos = y2;
  }
  return pos;
}

bool KIM101::pzmotMoveAbsolute(uint16_t channel, int32_t pos) {
  bool status = false;
  if (getStatus() != APTDevStatus::Initialized) return status;
  if (channel < 1 || channel > 4) return status;
  std::unique_lock<std::mutex> lock(moveMutex);
  if (channel == 1) {
    if (x1 == pos) {
      return true;
    }
  } else if (channel == 2) {
    if (y1 == pos) {
      return true;
    }
  } else if (channel == 3) {
    if (x2 == pos) {
      return true;
    }
  } else if (channel == 4) {
    if (y2 == pos) {
      return true;
    }
  }
  if (!enableChannel(channel)) {
      return false;
  }
  channel = (0x01 << (channel - 0x1));
  APT_MSG& msg = getMsg(APT_MSG_ID::PZMOT_MOVE_ABSOLUTE);
  msg.insertValueInBytes(6, 7, channel);
  msg.insertValueInBytes(8, 11, pos);
  ftStatus = write(msg);
  if (ftStatus != FT_OK) {
      return false;
  } 
  if (moveCompletedCV.wait_for(lock, std::chrono::seconds(5)) ==
      std::cv_status::timeout) {
      setError("No response from the device.");
      return false;
  }
  send(LIBAPT::APT_MSG_ID::PZMOT_REQ_STATUSUPDATE);
  return true;
}

bool KIM101::posCounts(uint16_t channel, int32_t pos) {
  bool status = false;
  if (getStatus() != APTDevStatus::Initialized) return status;
  APT_MSG& msg = getMsg(APT_MSG_ID::PZMOT_SET_PARAMS);
  msg.setSubMsg(APT_SUBMSG_ID::POSCOUNTERS,
                APT_SUBMSG_SZ::SET_POSCOUNTERS);
  uint16_t ch = 1 << (channel - 1);
  msg.insertValueInBytes(8, 9, ch);
  msg.insertValueInBytes(10, 13, pos);
  ftStatus = write(msg);
  if (ftStatus == FT_OK) status = true;
  return status;
}

bool KIM101::enableChannel(uint16_t channel) {
  if (getStatus() != APTDevStatus::Initialized) return false;
  APT_MSG& msg = getMsg(APT_MSG_ID::PZMOT_SET_PARAMS);
  msg.setSubMsg(APT_SUBMSG_ID::KCUBECHANENABLEMODE,
                APT_SUBMSG_SZ::KCUBECHANENABLEMODE);
  if (channel > 0x06) {
    setError("Wrong channel provided\n");
    return false;
  }
  msg.insertValueInBytes(8, 9, channel);
  ftStatus = write(msg);
  if (ftStatus!=FT_OK) return false;
  return true;
}
};  // namespace LIBAPT
