#include <apt.hpp>
#include <apt_msg.hpp>

namespace LIBAPT {
  KDC101::KDC101(std::string sn) {
  if (serialNumber.empty()) {
    serialNumber = sn;
  }
  devType = APTDevType::KDC101;
  
  addMsg(APT_MSG_ID::HW_DISCONNECT);
  addMsg(APT_MSG_ID::HW_RESPONSE);
  addMsg(APT_MSG_ID::HW_RICHRESPONSE);
  addMsg(APT_MSG_ID::MOT_MOVE_ABSOLUTE, 12);
  addMsg(APT_MSG_ID::MOT_MOVE_RELATIVE, 12);
  addMsg(APT_MSG_ID::MOT_MOVE_HOME);
  addMsg(APT_MSG_ID::MOT_MOVE_HOMED);
  addMsg(APT_MSG_ID::MOT_REQ_DCSTATUSUPDATE);
  addMsg(APT_MSG_ID::MOT_GET_DCSTATUSUPDATE);
  addMsg(APT_MSG_ID::MOT_MOVE_COMPLETED);
  addMsg(APT_MSG_ID::MOT_MOVE_STOPPED);
  addMsg(APT_MSG_ID::MOT_SET_HOMEPARAMS, 20);
  addMsg(APT_MSG_ID::MOT_REQ_HOMEPARAMS);
  addMsg(APT_MSG_ID::MOT_GET_HOMEPARAMS);
  addMsg(APT_MSG_ID::MOT_REQ_STATUSBITS);
  addMsg(APT_MSG_ID::MOT_GET_STATUSBITS);

  hwRichResponse = [this](APT_MSG& msg) {
    uint16_t msgId;
    msg.extractValueFromBytes(6, 7, msgId);
    uint16_t code;
    msg.extractValueFromBytes(6, 7, code);
  };

  motMoveHomed = [this](APT_MSG& msg) { motMoveCV.notify_all(); };
  addMsgAction(APT_MSG_ID::MOT_MOVE_HOMED, motMoveHomed);

  motMoveCompleted = [this](APT_MSG& msg) { motMoveCV.notify_all(); };
  addMsgAction(APT_MSG_ID::MOT_MOVE_COMPLETED, motMoveCompleted);

  motMoveStopped = [this](APT_MSG& msg) { motMoveCV.notify_all(); };
  addMsgAction(APT_MSG_ID::MOT_MOVE_STOPPED, motMoveStopped);

  motGetHomeParams = [this](APT_MSG& msg) {
    msg.extractValueFromBytes(8,  9, homeDir);
    msg.extractValueFromBytes(10, 11, limitSwitch);
    msg.extractValueFromBytes(12, 15, homeVelocity);
    msg.extractValueFromBytes(16, 19, offsetDistance);
    posCV.notify_all();
  };
  addMsgAction(APT_MSG_ID::MOT_GET_HOMEPARAMS, motGetHomeParams);

  motGetDCStatusUpdate = [this](APT_MSG& msg) {
    msg.extractValueFromBytes(8, 11, currentPos);
    msg.extractValueFromBytes(12, 13, currentVel);
    msg.extractValueFromBytes(16, 19, statusBits);
    posCV.notify_all();
  };
  addMsgAction(APT_MSG_ID::MOT_GET_DCSTATUSUPDATE, motGetDCStatusUpdate);
};

auto KDC101::init() -> bool {
  if (getStatus() == APTDevStatus::Initialized) {
    return true;
  }
  if (!APTDev::init()) {
    return false;
  }
  send(LIBAPT::APT_MSG_ID::HW_REQ_INFO);
  // send(LIBAPT::APT_MSG_ID::HW_START_UPDATEMSGS);
  send(LIBAPT::APT_MSG_ID::MOT_REQ_DCSTATUSUPDATE);
  std::this_thread::sleep_for(std::chrono::milliseconds(150));
  return true;
}

float KDC101::getPosition() {
  if (getStatus() != APTDevStatus::Initialized) {
    setError("Device not initialized");
    return -9999.0;
  }
  send(LIBAPT::APT_MSG_ID::MOT_REQ_DCSTATUSUPDATE);
  std::unique_lock<std::mutex> lock(moveMutex);
  if (posCV.wait_for(lock, std::chrono::seconds(5)) == std::cv_status::timeout) {
	return -9999.0;
  }
  // posCV.wait(lock);
  return currentPos / static_cast<float>(encCount);
}

bool KDC101::motMoveRelative(uint8_t channel) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_RELATIVE);
  if (msg.size() != 6) msg.setSize(6);
  msg.insertValueInBytes(3, channel);
  if (!write(msg)) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  motMoveCV.wait(lock);
  return true;
}

bool KDC101::motMoveAbsolute(uint8_t channel) {
  bool status = false;
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_ABSOLUTE);
  msg.insertValueInBytes(3, channel);
  if (write(msg) != FT_OK) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  if (motMoveCV.wait_for(lock, std::chrono::seconds(30)) == std::cv_status::timeout) {
    return false;
  }
  return true;
}

bool KDC101::motMoveRelative(uint16_t channel, double relPosMM) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_RELATIVE);

  int32_t position = relPosMM * encCount;  // Convert mm to counts
  if (msg.size() != 12) msg.setSize(12);
  msg.insertValueInBytes(6, 7, channel);
  msg.insertValueInBytes(8, 11, position);
  if (write(msg) != FT_OK) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  if (motMoveCV.wait_for(lock, std::chrono::seconds(30)) == std::cv_status::timeout) {
    return false;
  }
  return true;
}

bool KDC101::motMoveAbsolute(uint16_t channel, double absPosMM) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_ABSOLUTE);
  uint32_t position = absPosMM * encCount;  // Convert mm to counts
  if (msg.size() != 12) msg.setSize(12);
  msg.insertValueInBytes(6, 7, channel);
  msg.insertValueInBytes(8, 11, position);
  if (write(msg) != FT_OK) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  if (motMoveCV.wait_for(lock, std::chrono::seconds(30)) == std::cv_status::timeout) {
    send(LIBAPT::APT_MSG_ID::HW_STOP_UPDATEMSGS);
    return false;
  }
  return true;
}

bool KDC101::setHomeParams(uint16_t channel, uint16_t hD) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  send(APT_MSG_ID::MOT_REQ_HOMEPARAMS);
  std::unique_lock<std::mutex> lock(moveMutex);
  if (posCV.wait_for(lock, std::chrono::seconds(30)) == std::cv_status::timeout) {
    return false;
  };
  std::this_thread::sleep_for(std::chrono::milliseconds(150));
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_SET_HOMEPARAMS);
  msg.insertValueInBytes(3, channel);
  msg.insertValueInBytes(8, 9, hD);
  msg.insertValueInBytes(10, 11, 1);
  msg.insertValueInBytes(12, 15, homeVelocity);
  msg.insertValueInBytes(16, 19, offsetDistance);
  if (write(msg) != FT_OK) {
    return false;
  }
  return true;
}
bool KDC101::reqHomeParams(uint16_t channel) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_REQ_HOMEPARAMS);
  msg.insertValueInBytes(3, channel);
  if (write(msg) != FT_OK) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  if (posCV.wait_for(lock, std::chrono::seconds(60)) == std::cv_status::timeout) {
    return false;
  }
  return true;
}

bool KDC101::motMoveHome(uint16_t channel) {
  if (getStatus() != APTDevStatus::Initialized) {
    setError ("Device not initialized");
    return false;
  };
  APT_MSG& msg = getMsg(APT_MSG_ID::MOT_MOVE_HOME);
  if (write(msg) != FT_OK) {
    return false;
  }
  std::unique_lock<std::mutex> lock(moveMutex);
  if (motMoveCV.wait_for(lock, std::chrono::seconds(60)) == std::cv_status::timeout) {
    setError("Homing not finished");
    return false;
  }
  return true;
}
};
