// -*- lsst-c++ -*-
#ifndef APT_H__
#define APT_H__

#include <ftd2xx.h>
#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <future>
#include <iostream>
#include <map>
#include <sstream>
#include <thread>
#include <vector>
#include <cstring>
#include <memory>
#include <cstdint>

#include "apt_msg.hpp"

#define VID 0x0403
#define PID 0xFAF0

#define LIBAPT_VERSION_MAJOR 0
#define LIBAPTL_VERSION_MINOR 1
#define LIBAPT_VERSION ((LIBAPT_VERSION_MAJOR << 16) | LIBAPT_VERSION_MINOR)

namespace LIBAPT {

enum class APTDevStatus { NotFound, Disabled, Initialized };

inline std::ostream& operator<<(std::ostream& out, APTDevStatus status) {
  switch (status) {
    case APTDevStatus::NotFound:    out << "Not Found"; break;
    case APTDevStatus::Disabled:    out << "Disabled"; break;
    case APTDevStatus::Initialized: out << "Initialized"; break;
    default:                        out << int(status); break;
  }
  return out;
}

/** @enum APTDevType
 *  @brief shows the type of device as reported by the Serial Number prefix
 *  The codes for KDC101 and MFF have been taken from APT Host-Controller 
 *  Communication Protocol, while the code for KIM101 has been found by scanning
 *  the device.
 */
enum class APTDevType : unsigned int {
  KDC101 = 27,
  MFF = 37,
  KIM101 = 97,
};

inline std::ostream& operator<<(std::ostream& out, APTDevType devtype) {
  switch (devtype) {
    case APTDevType::MFF:    out << "MFF"; break;
    case APTDevType::KIM101: out << "KIM101"; break;
    case APTDevType::KDC101: out << "KDC101"; break;
    default:                 out << int(devtype); break;
  }
  return out;
}

/**
 * Try to find the devices connected to the host computer.
 * @param serialNumbers Container with the serial numbers found.
 * @param devDesc Container with the type of each device found.
 */
template <typename C>
bool aptDevScan(C* connectedDevices = nullptr) {
  FT_STATUS ftStatus;

  // Set the VID:PID combination, necessary since the driver does not include
  // this combination by default. To find it, run lsusb and find "Future
  // Technology Devices International"
  ftStatus = FT_SetVIDPID(VID, PID);
  if (ftStatus != FT_OK) {
    return false;
  }

  DWORD numDevs;
  ftStatus = FT_CreateDeviceInfoList(&numDevs);
  if (ftStatus != FT_OK) {
    return false;
  }

  FT_DEVICE_LIST_INFO_NODE* devInfo;
  devInfo = new FT_DEVICE_LIST_INFO_NODE[numDevs];
  ftStatus = FT_GetDeviceInfoList(devInfo, &numDevs);
  if (ftStatus != FT_OK) {
    return false;
  }

  for (unsigned int i = 0; i < numDevs; i++) {
    if (connectedDevices == nullptr)
      return false;
    std::string sn = devInfo[i].SerialNumber;
    //Extract the APTDevType from the SerialNumber, as explained in the enum
    unsigned int t = std::stoi(sn.substr(0,2));
    APTDevType type = static_cast<APTDevType>(t);
    std::map<std::string, APTDevType>* map = connectedDevices;
    (map)->insert(std::make_pair(sn, type));
  }

  delete[] devInfo;
  return true;
}

/** @class APTDev
 *  @brief Base class for all APT Devices
 */
class APTDev {
 friend class APTDevFactory;
 public:
  APTDev();
  virtual ~APTDev();

  /**
   * Function initializing the device.
   * @param id The serial number of the device, required if more that one devices are connected
   * @param verbose Enable the printing of the messages sent to, or received by, the device.
   */
  virtual bool init();

  /**
   * Function closing the connection to the device.
   */
  bool finalize();

  /**
   * Function returning the error found at the device.
   */
  std::string getError();

  /**
   * Function sending the message to the device.
   * @param header the message to send
   */
  auto send(APT_MSG_ID header = APT_MSG_ID::EMPTY) -> bool;

  ///Return the serial number as reported after sending HW_REQ_INFO
  unsigned long getSerialNumber();
  ///Return the model name as reported after sending HW_REQ_INFO
  std::string getModelName();
  ///Return the hardware type as reported after sending HW_REQ_INFO
  uint16_t getHardwareType();
  ///Return the firmware version as reported after sending HW_REQ_INFO
  uint32_t getFirmwareVersion();
  ///Return the hardware version as reported after sending HW_REQ_INFO
  uint16_t getHardwareVersion();
  ///Return the modification state as reported after sending HW_REQ_INFO
  uint16_t getModificationState();
  ///Return the number of channels as reported after sending HW_REQ_INFO
  uint16_t getNumberChannels();

  ///Return the status of the device
  inline APTDevStatus getStatus() const {return devStatus; };
  ///Return the type of the device
  inline APTDevType getType() const { return devType; };
  ///Return the current verbosity level of the device
  inline auto getVerbosity() -> bool { return verbose; };
  ///Modify the verbosity level of the device
  auto setVerbosity(bool v) -> bool;

 protected:
  FT_STATUS ftStatus = FT_OK; ///Status of the device reported by D2XX
  FT_HANDLE ftHandle = nullptr; ///Handle to communicate through D2XX 
  std::string serialNumber; ///Serial number as set by the user
  APTDevType devType; ///Type of the devide
  FT_STATUS write(APT_MSG& msg, HOSTS source = HOSTS::USB,
                  HOSTS destination = HOSTS::HOST);

  ///Set an error for the device. Used when we get an ftStatus != FT_OK
  auto setError(const std::string& e) -> void { error = e; };
  ///add a message to the list of supported messages
  auto addMsg(APT_MSG_ID msg, size_t size = APT_MSG_SZ::HEADER) -> void;
  ///Search the list of supported messages and return the message
  auto getMsg(APT_MSG_ID msg) -> APT_MSG&;
  ///Connect the receivement of a message to a lambda call
  auto addMsgAction(APT_MSG_ID msgId, msgAction action) -> void;

 private:
  bool verbose = false;
  std::string error = "";
  std::map<APT_MSG_ID, APT_MSG> messagelist;

  unsigned long hwSerialNumber;
  std::string hwModelName = "";
  uint16_t hwType = 0;
  unsigned int hwFirmwareVersion = 0;
  unsigned int hwHardwareVersion = 0;
  unsigned int hwModificationState = 0;
  unsigned int hwNumberChannels = 0;
  std::mutex mtx;
  std::condition_variable hwReqInfoCV;
  bool hwReqInfo = false;
  APTDevStatus devStatus = APTDevStatus::Disabled;

  /// Variables and functions for the device monitoring.
  FT_STATUS checkRxQueue();
  std::vector<uint8_t> rxBuffer;

  // std::promise<void> exitThreadsSignal;
  // std::future<void> processingActive;
  int processingActive2 = 0;
  std::thread deviceMonitorThread;
  void deviceMonitor(std::future<void> processingActive);
  void deviceMonitor2(int* processingActive);
};

/** @class KDC101
 *  @brief support for KDC101 devices.
 */
class KDC101 : public APTDev {
 friend class APTDevFactory;
 public:
  /// Constructor used for initialization of messages
  KDC101(std::string sn = "");
  bool init() override;
  float getPosition();
  // Functions for relative moves
  bool motMoveRelative(uint8_t channel = 0);
  bool motMoveRelative(uint16_t channel = 0, double absPosition = 0);
  bool motMoveAbsolute(uint8_t channel = 0);
  bool motMoveAbsolute(uint16_t channel = 0, double absPosition = 0);
  bool motMoveHome(uint16_t channel = 0);
  bool setHomeParams(uint16_t channel = 0, uint16_t homeDir = 1);
  bool reqHomeParams(uint16_t channel = 0);

 private:
  const int encCount = 34304;
  int32_t currentPos = 0;
  uint32_t currentVel = 0;
  uint32_t statusBits = 0;

  int16_t homeDir = 0;
  int16_t limitSwitch = 0;
  uint32_t homeVelocity = 0;
  uint32_t offsetDistance = 0;

  // actions
  msgAction hwRichResponse;
  msgAction motGetDCStatusUpdate;
  msgAction motMoveCompleted;
  msgAction motMoveStopped;
  msgAction motMoveHomed;
  msgAction motGetHomeParams;

  std::mutex moveMutex;
  std::condition_variable motMoveCV;
  std::condition_variable posCV;
};

/** @class MFF
 *  @brief support for MFF devices.
 */
class MFF : public APTDev {
 friend class APTDevFactory;
 public:
  /// Constructor used for initialization of messages
  MFF(std::string sn = "");
  bool init() override;

  enum class position : uint8_t {
    down = 0x01,
    up = 0x02,
    toggle = 0x03,
    motion = 0x10,
    unknown1 = 0x12,
  };
  position getPosition();
  std::string getPositionName();
  bool motMoveJog(uint8_t channel, MFF::position direction);
  bool motSetMFFOperparams();
  bool setITransitTime(uint32_t time);
  bool setITransitTimeADC(uint32_t time);
  bool setOperMode1(uint16_t mode);
  bool setSignalMode1(uint16_t mode);
  bool setPulseWidth1(uint32_t time);
  bool setOperMode2(uint16_t mode);
  bool setSignalMode2(uint16_t mode);
  bool setPulseWidth2(uint32_t width);

  uint32_t getITransitTime();
  uint32_t getITransitTimeADC();
  uint16_t getOperMode1();
  uint16_t getSignalMode1();
  uint32_t getPulseWidth1();
  uint16_t getOperMode2();
  uint16_t getSignalMode2();
  uint32_t getPulseWidth2();

 private:
  // actions
  msgAction motGetMFFOperparams;
  msgAction motGetStatusUpdate;

  // internal functions
  bool mffOperParamsReady = false;
  bool mffOperParamsPrepared();
  bool setOperMode(int ioChannel, uint16_t mode);
  bool setSignalMode(int ioChannel, uint16_t mode);
  bool setPulseWidth(int ioChannel, uint32_t mode);

  // parameter limits
  const uint32_t iTransitTimeMin = 300;
  const uint32_t iTransitTimeMax = 2800;
  const uint32_t iTransitTimeADCMin = 32;
  const uint32_t iTransitTimeADCMax = 1147;
  const uint32_t pulseWidthMin = 10;
  const uint32_t pulseWidthMax = 200;
  const uint16_t operModeMin = 0x00;
  const uint16_t operModeMax = 0x40;
  const std::vector<int> signalModeValues = {0x01, 0x02, 0x04,
                                             0x10, 0x20, 0x40};

  // Parameters that can be set
  position pos = position::down;
  uint32_t iTransitTime = 0;
  uint32_t iTransitTimeADC = 0;
  uint16_t operMode1 = 0;
  uint16_t sigMode1 = 0;
  uint32_t pulseWidth1 = 0;
  uint16_t operMode2 = 0;
  uint16_t sigMode2 = 0;
  uint32_t pulseWidth2 = 0;

  // Mutexes for the messages
  std::mutex moveMutex;
  std::mutex mffOperParamsMutex;
  bool moveInitiated = false;
  std::condition_variable motMoveCV;
  std::condition_variable mffOperParamsCV;
};

/** @class KIM101
 *  @brief support for KIM101 devices.
 */
class KIM101 : public APTDev {
 friend class APTDevFactory;
 public:
  KIM101(std::string sn = "");
  bool init() override;
  int32_t getPosition(uint16_t channel);
  bool pzmotMoveAbsolute(uint16_t chanel, int32_t pos);
  bool enableChannel(uint16_t channel);
  bool posCounts(uint16_t channel, int32_t pos);
 private:
  /// Constructor used for initialization of messages
  msgAction pzmotGetStatusUpdate;
  msgAction pzmotMoveCompleted;
  int32_t x1;
  int32_t y1;
  int32_t x2;
  int32_t y2;
  std::mutex moveMutex;
  std::condition_variable moveCV;
  std::condition_variable moveCompletedCV;
};

/** @class APTDevFactory
 *  @brief Factory generator of APTDev children.
 */
class APTDevFactory {
  std::map<APTDevType, std::function<std::unique_ptr<APTDev>(const std::string&)>> m_factories;
  public:
    APTDevFactory() {
      m_factories[APTDevType::MFF]  = [](const std::string& sn) {
	return std::make_unique<MFF>(sn);
      };
      m_factories[APTDevType::KIM101] = [](const std::string& sn) {
	return std::make_unique<KIM101>(sn);
      };
      m_factories[APTDevType::KDC101] = [](const std::string& sn) {
	return std::make_unique<KDC101>(sn);
      };
    }

  auto create(APTDevType type, std::string sn) ->std::shared_ptr<APTDev> {
    return m_factories[type](sn);
  }
};

}  // namespace LIBAPT
#endif  // APT_H__
